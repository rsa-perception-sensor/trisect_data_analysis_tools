from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

d = generate_distutils_setup(
    packages=['trisect_data_analysis_tools'],
    scripts=['scripts/trisect_timing_from_rosbag',
             "scripts/trisect_calibration_ros.py"]
    #    package_dir={'': 'src'}
)

setup(**d)
