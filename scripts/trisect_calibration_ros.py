#! /usr/bin/env python3

# Author: Laura Lindzey & Aaron Marburg
# Copyright 2020-2022 University of Washington Applied Physics Laboratory
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its
#    contributors may be used to endorse or promote products derived from
#    this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
# THE POSSIBILITY OF SUCH DAMAGE.

# Customized version of charcuo_stereo_calibration/scripts/run_calibration_ros.py
# For Trisect camera
#
# See the sample launch file in the launch/ directory in this repo

# Right now it's a little cut-and-paste repetitive with run_calibration_ros.py

from __future__ import print_function
from future.utils import iteritems

import copy
import os
import pickle

import numpy as np

import cv2
import cv2.aruco

import cv_bridge
import rosbag
import rospy
from sensor_msgs.msg import Image
from collections import namedtuple

import charuco_utils
from charuco_stereo_calibration import calibration
from charuco_stereo_calibration import plot_calibration

CalibrationData = namedtuple(
    "CalibrationData", ["index", "image_size", "camera"])
Camera = namedtuple("Camera", ["name", "topic"])


class TrisectCharucoCalibrator(object):
    def __init__(self):
        # If not None, a number of the steps will save debug figures
        self.figure_dir = rospy.get_param("~figdir")
        self.meta_dir = rospy.get_param("~metadir")

        self.generate_board()

        # Generate image indices
        self.bagdir = rospy.get_param("~bagdir")

        self.process_color = rospy.get_param("~process_color",False)

        left_cam = Camera("left", rospy.get_param("~left_topic"))
        right_cam = Camera("right", rospy.get_param("~right_topic"))

        self.left_data = self.generate_index(left_cam)
        self.right_data = self.generate_index(right_cam)

        if self.process_color:
            color_cam = Camera("color", rospy.get_param("~color_topic"))
            self.color_data = self.generate_index(color_cam)

    def run_calibration(self):
        self.run_intrinsic_calibration(self.left_data)
        self.run_intrinsic_calibration(self.right_data)

        self.run_stereo_calibration(self.left_data, self.right_data)

        if self.process_color:
            self.run_intrinsic_calibration(self.color_data)

            self.run_stereo_calibration(self.left_data, self.color_data)
            self.run_stereo_calibration(self.color_data, self.right_data)

    def generate_board(self):

        board_name = rospy.get_param("~charuco_board_name")

        self.aruco_dictionary, self.charuco_board = charuco_utils.get_charuco_board(board_name)

        # # Generate aruco dictionary / charuco board
        # ncols = rospy.get_param("~checkerboard_columns")
        # nrows = rospy.get_param("~checkerboard_rows")
        # checker_size = rospy.get_param("~checker_size")

        # # In this dictionary, each white square has:
        # # * 1 pixel white border
        # # * 1 pixel black border (counts as part of the aruco marker size)
        # # * 4x4 pixel aruco marker
        # # TODO: If we start using other dictionaries, make these parameters!
        # aruco_dict_name = cv2.aruco.DICT_4X4_250
        # aruco_size = 0.75 * checker_size

        # self.aruco_dictionary, self.charuco_board = calibration.setup_charuco_board(
        #     aruco_dict_name, ncols, nrows, checker_size, aruco_size)

        if self.figure_dir is not None:
            if not os.path.isdir(self.figure_dir):
                os.makedirs(self.figure_dir)
            plot_calibration.plot_aruco_markers(self.aruco_dictionary,
                                                self.figure_dir, 5, 10)
            plot_calibration.plot_charuco_board(self.charuco_board,
                                                self.figure_dir)

    def generate_index(self, camera):
        """
        Try loading image index; if it doesn't exist, create it.
        (helper function for generate_monocular_index, generate_stereo_indices)

        Generating the index and saving images can take over an hour, so it
        should only be done once.

        NOTE: Using pickle is a bit fragile -- I think it'll fail to load the
            index if I change how calibration is imported.

        @param bagdir: index will be generated of bagdir/*.bag
        @param index_filename: filename to load/save index as
        @param image_topic: index wil be of all sensor_msgs/Image messages
                            matching this topic
        @param prefix: directory prefix for saved images (within self.figure_dir)
        """

        index_filename = rospy.get_param("~%s_index_filename" % camera.name,
                                         "%s/%s_image_index.pkl" % (self.meta_dir, camera.name))
        print("\nGenerating index, looking for existing file\n * {}\n"
              .format(index_filename))

        try:
            index = pickle.load(open(index_filename, 'rb'))
        except Exception as ex:
            print("Unable to load {}".format(index_filename))
            print()
            print(ex)
            print()
            print("Generating index file -- this will be slow!!")
            index = calibration.generate_image_index(
                self.bagdir, camera.topic, self.aruco_dictionary, self.charuco_board,
                "{}/{}_images".format(self.figure_dir, camera.name), save_images=True)
            pickle.dump(index, open(index_filename, 'wb'))

        image_size = calibration.get_image_size(self.bagdir, camera.topic)
        return CalibrationData(index, image_size, camera)

    def run_intrinsic_calibration(self, calib_data):

        bad_seqs = rospy.get_param("~bad_%s_seqs" % calib_data.camera.name)

        # NB: The filename-formatting logic is duplicated in run_stereo_calibration
        calib_filename = "{}/{}_intrinsic_params.yaml".format(
            self.meta_dir, calib_data.camera.name)

        if os.path.exists(calib_filename):
            print("Skipping intrinsic calibration, since {} exists.".format(
                calib_filename))
            return

        num_images = rospy.get_param("~num_intrinsic_images")
        min_corners = rospy.get_param("~min_corners")

        seqs = calibration.select_evenly_spaced(
            calib_data.index, num_images, min_corners, bad_seqs)

        if self.figure_dir is not None:
            fig_filename = "{}/{}_pose_distribution.png".format(
                self.figure_dir, calib_data.camera.name)

            plot_calibration.plot_pose_distributions(calib_data.index, [('c', seqs)],
                                                     filename=fig_filename)

        print("Calculating intrinsics for %s" % calib_data.camera.name)
        output = calibration.calibrate_mono(self.charuco_board, calib_data.image_size,
                                            calib_data.index, seqs)

        if output:
            err, ii, dd, _, _, _, _, _ = output
            calibration.save_calibration_mono(
                calib_filename, calib_data.camera.name, dd, ii, calib_data.image_size)

            _, _, _, _, _, _, _, per_view_errors = output

            print("")
            print("Intrinsic calibration errors for {}:".format(
                calib_data.camera.name))
            print("  average reprojection error: {}".format(err))
            print("  Images with reprojection error > 1.0:")
            for error, seq in zip(per_view_errors, seqs):
                filename = calib_data.index[seq].image_filepath.split('/')[-1]
                if error > 1.0:
                    print("    error: {}, image: {}".format(error, filename))
                figname = "{}_image_{:06d}_error_{:03d}.png".format(
                    calib_data.camera.name,
                    seq, int(100*error))
                print("       saving debug image to : {}".format(figname))
                plot_calibration.plot_image_detections(calib_data.index[seq],
                                                       "{}/{}".format(self.figure_dir, figname))
        else:
            print("No valid images found!")
            exit()

    def run_stereo_calibration(self, left, right):

        num_images = rospy.get_param("~num_stereo_images")
        min_corners = rospy.get_param("~min_corners")
        max_dt = 0.01  # seconds
        stereo_seqs = calibration.select_stereo_evenly_spaced(
            left.index, right.index, num_images, min_corners, max_dt)

        left_stereo_filename = "{}/{}_stereo_params.yaml".format(
            self.meta_dir, left.camera.name)
        right_stereo_filename = "{}/{}_stereo_params.yaml".format(
            self.meta_dir, right.camera.name)

        # TODO: Logic for formatting filename is duplicated with _calibrate_intrinsic
        left_intrinsic_filename = "{}/{}_intrinsic_params.yaml".format(
            self.meta_dir, left.camera.name)
        right_intrinsic_filename = "{}/{}_intrinsic_params.yaml".format(
            self.meta_dir, right.camera.name)
        transform_filename = "{}/{}_{}_transform.yaml".format(
            self.meta_dir, left.camera.name, right.camera.name)

        # Now **load** the intrinsics
        left_params = calibration.CalibrationParams(left_intrinsic_filename)
        right_params = calibration.CalibrationParams(right_intrinsic_filename)

        if left.image_size == right.image_size:
            err = calibration.calibrate_stereo(self.charuco_board, stereo_seqs,
                                               left.image_size,
                                               left.index, right.index,
                                               left_params, right_params,
                                               left_stereo_filename, right_stereo_filename,
                                               transform_filename=transform_filename)

            print("Calibrated %s to %s. Reprojection error = %.4f" %
                  (left.camera.name, right.camera.name, err))
        else:
            print("%s and %s image sizes are different, doing limited calibration" %
                  (left.camera.name, right.camera.name))
            err = calibration.calibrate_stereo(self.charuco_board, stereo_seqs,
                                               left.image_size,
                                               left.index, right.index,
                                               left_params, right_params, do_rectify=False,
                                               transform_filename=transform_filename)


if __name__ == "__main__":
    rospy.init_node("trisect_calibration")

    calibrator = TrisectCharucoCalibrator()
    calibrator.run_calibration()
